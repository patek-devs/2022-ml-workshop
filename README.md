# ML workshop
## Jak začít?
Vytvořte si virtualenv:

```
python3 -m venv env
```

vlezte do něj:

```
source env/bin/activate
```

nainstalujte requirements:

```
python3 -m pip install -r requirements.txt
```
